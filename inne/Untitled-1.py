#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# print("Alfabet w porządku naturalnym:")
# x = 0
# for i in range(65, 91):
#     litera = chr(i)
#     x += 1
#     tmp = litera + " => " + litera.lower()
#     if i > 65 and x % 5 == 0:
#         x = 0
#         tmp += "\n"
#     print(tmp, end=" ")

# x = -1
# print("\nAlfabet w porządku odwróconym:")
# for i in range(122, 96, -1):
#     litera = chr(i)
#     x += 1
#     if x == 5:
#         x = 0
#         print("\n", end=" ")
#     print(litera.upper(), "=>", litera, end=" ")


# def przywitanie_imienne(imie, zyczenia):
#     print( "Witaj " + imie + ". Zycze Tobie " + zyczenia)

# przywitanie_imienne( "Dagmara"," dzień dobry")   

def podniesc_do_potegi(x, p=2):
    print( x ** p )
    return "prawie prawie"

# print(podniesc_do_potegi(4))
# podniesc_do_potegi(2,1)
# podniesc_do_potegi(2,2)
# podniesc_do_potegi(2,3)
# podniesc_do_potegi(2,4)
# podniesc_do_potegi(2,5)
# podniesc_do_potegi(2,6)
# podniesc_do_potegi(2,7)
# podniesc_do_potegi(2,8)
# podniesc_do_potegi(2,9)



def silnia(x):
    if x == 1:
        return 1
    print( "teraz licze dla  " + str(x) )
    return silnia(x-1) * x
# print (silnia(10))

def lancuchZnakow():
    