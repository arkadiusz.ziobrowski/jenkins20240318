﻿def convert(string):
    try:
        return float(string)
    except ValueError:
        print("Nie można skonwertować łańcucha na liczę zmiennoprzecinkową.")

c = convert("5525223652325655526323e100")
print(c)
